﻿DROP DATABASE IF EXISTS ejemplo2Yii;
CREATE DATABASE ejemplo2Yii;
USE ejemplo2Yii;


-- Creación de tablas

CREATE OR REPLACE TABLE libros(
  id int AUTO_INCREMENT,
  titulo varchar(200),
  sinopsis varchar(200),
  fecha date,
  autor int,
  PRIMARY KEY(id)
  );


CREATE OR REPLACE TABLE autores(
  id int AUTO_INCREMENT,
  nombre varchar(200),
  email varchar(50),
  fechaNacimiento date,
  PRIMARY KEY(id)
);

-- Creación de constraints

ALTER TABLE libros
  ADD CONSTRAINT fk_libros_autores 
  FOREIGN KEY (autor) REFERENCES autores(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

-- ON DELETE CASCADE va a borrar todos los libros de un autor cuando se borre el autor
-- ON update CASCADE va a actualizar todos los autores de un libro cuando se actualice en la tabla autores

