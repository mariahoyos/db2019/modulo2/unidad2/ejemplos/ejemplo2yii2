<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Autores */

//$this->title = $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Autores', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
//\yii\web\YiiAsset::register($this);
?>
<div class="autores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'email:email',
            'fechaNacimiento',
            'imagen',
            [
                'label'=>'foto del autor',
                'format'=>'raw',
                'value'=>function($model){
                    return Html::img('@web/imgs/' . $model->imagen,['class'=>'img-thumbnail']);
                }
            ],
        ],
    ]) ?>

</div>
