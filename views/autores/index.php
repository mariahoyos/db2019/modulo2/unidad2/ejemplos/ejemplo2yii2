<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Autores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'email:email',
            'fechaNacimiento',
            'imagen',
            [
                'label'=>'foto del autor',
                'format'=>'raw',
                'value'=>function($model){
                    return Html::img('@web/imgs/' . $model->imagen,['class'=>'img-thumbnail']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{view}{update}{delete}{libros}',    
            'buttons'=>[
                'libros'=>function($url,$model){
                    return Html::a('<span class = "glyphicon glyphicon-book"></span>',['libros/libros','id'=>$model->id]);
                    
                },
            ],   
                
                ]
        ],
    ]); ?>


</div>
