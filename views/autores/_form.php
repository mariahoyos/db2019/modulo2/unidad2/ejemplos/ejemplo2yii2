<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Autores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="autores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php
    
    echo '<label class="control-label">Birth Date</label>';
    echo DatePicker::widget([
        'model' => $model, 
        'attribute' => 'fechaNacimiento',
        'options' => ['placeholder' => 'Introduzca la fecha de nacimiento'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'yyyy-m-dd',
            'autoclose' => true,
        ]
    ]);
    ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



