<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Libros del autor: '.$autor->nombre;
/* memoriza la dirección de donde vienes */
$atras=Yii::$app->request->referrer;
/*modifica los breadcrumbs para volver a la página en la que estábamos anteriormente*/
$this->params['breadcrumbs'][] = ['label'=>'Autores','url'=>$atras];

/* Crea una entrada en el array de migas de pan al final con un push */
//$this->params['breadcrumbs'][] = ['label'=>'Autores','url'=>['autores/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?=
    /* cargar una vista de otro controlador */
    /* Para cambiar entre carpeta usamos // para ir a la carpeta superior
     * Además el detail view espera un parámetro que se llama model */
    $this->render('//autores/_view',[
    'model'=>$autor
]) ?>

<div class="libros-index">

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'titulo',
            'sinopsis',
            'fecha',
            'autor',
            
        ],
    ]); ?>


</div>
